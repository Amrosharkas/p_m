<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Company_holiday;
use App\Holiday;
use App\User;
use App\User_holiday;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HolidayController extends Controller
{
    public function updateCompanyHolidays(Request $request){
    	$data = $request->input();
    	unset($data['_token']);
    	//$data['week_days'] = substr($data['week_days'],1);
    	$check_count = Company_holiday::where('company_id',$data['company_id'])->count();
    	if($check_count == 0){
    		Company_holiday::create($data);
    	}else{
    		Company_holiday::where('company_id',$data['company_id'])->update($data);
    	}

    }

    public function getHolidays(){
        $current_user = Auth::user();
        $users = Holiday::where('company_id',$current_user->company_id)->get();
        $model_name = "Holiday";
        return view('admin.get_holidays',compact('users','model_name'));

    }
    public function holidayDetails($id){
        $current_user = Auth::user();
        $user = Holiday::find($id);
        $users = User::where('company_id',$current_user->company_id)->get();
        return view('admin.holiday_details',compact('user','users'));

    }

    public function save(Request $request){
        $data = $request->input();
        $current_user = Auth::user();
        $selected_users_id = count($data["users"]);
        if($request->input('id') ==""){
            
            $holiday = Holiday::create($data);
            $create = array();
            for($i=0; $i<$selected_users_id; $i++) 
            {
                
                

                $user_id = trim(strip_tags($data["users"][$i]));
                $create['company_id'] = $current_user->company_id;
                $create['user_id'] = $user_id;
                $create['holiday_id'] = $holiday->id;
                
                User_holiday::create($create);
               
                
            }
        }else{
            
            $user = Holiday::find($request->input('id'))->update($data);
        }
        return 'Success';

    }
}
