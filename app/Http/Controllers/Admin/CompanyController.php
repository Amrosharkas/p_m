<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Company;
use App\User;
use App\Company_holiday;
use Hash;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CompanyController extends Controller
{
    public function getCompanies(){

		$users = Company::get();
		$model_name = "Company";
		return view('admin.get_companies',compact('users','model_name'));

    }
    public function companyDetails($id){

		$user = Company::find($id);
		return view('admin.company_details',compact('user'));

    }

    public function save(Request $request){
		$data = $request->input();
 		
    	if($request->input('id') ==""){
    		
			$user = Company::create($data);

			echo $user->id;
			$data['company_id'] = $user->id;
			$data['password'] = Hash::make($data['password']);
			$data['user_type'] = 'Owner';
			
			$insert_user = User::create($data);
			unset($data['password']);
			unset($data['name']);
			unset($data['email']);
			$data['week_days'] = ',Saturday,Friday';
			$insert_holidays =Company_holiday::create($data);
		}else{
			
			$user = Company::find($request->input('id'))->update($data);
		}
		return 'Success';

    }
}
