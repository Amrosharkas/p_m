<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;
use App\Project;
use App\Task;
use App\Client;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
     public function getProjects(){
     	$current_user = Auth::user();
		$users = Project::where('company_id',$current_user->company_id)->get();
		$model_name = "Project";
		return view('admin.get_projects',compact('users','model_name'));

    }
    public function projectDetails($id){
    	$current_user = Auth::user();
    	$clients = Client::where('company_id',$current_user->company_id)->get();
		$user = Project::find($id);
		return view('admin.project_details',compact('user','clients'));

    }

    public function save(Request $request){
		$data = $request->input();
 		
    	if($request->input('id') ==""){
    		
			$user = Project::create($data);
			
			return $user->id;
			
		}else{
			
			$user = Project::find($request->input('id'))->update($data);
			return $request->input('id');
		}
		

    }

    public function getGantt($project_id,$type){
    	$project = Project::find($project_id);
        if($type == "Estimated"){
        	$tasks = Task::where('project_id',$project_id)->orderBy('task_order','asc')->get();
        	$links = Task::where('project_id',$project_id)->where('dependency_id','>',0)->get();
        }else{
            $tasks = Task::where('project_id',$project_id)->where('act_start_date', '!=' ,'0000-00-00 00:00:00')->where('act_end_date', '!=' ,'0000-00-00 00:00:00')->orderBy('task_order','asc')->get();
            $links = Task::where('project_id',$project_id)->where('act_start_date', '!=' ,'0000-00-00 00:00:00')->where('act_end_date', '!=' ,'0000-00-00 00:00:00')->where('dependency_id','>',0)->get();
        }
        
    	return view('admin.gantt',compact('project','tasks','links','type'));
    }
}
