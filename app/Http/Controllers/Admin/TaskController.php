<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Project;
use App\Task;
use App\Dependency_type;
use App\User;
use App\Holiday;
use App\Company_holiday;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use mail;

class TaskController extends Controller
{
    public function getTasks(){
     	$current_user = Auth::user();
     	if($current_user->user_type == "Owner" || $current_user->user_type == "Super User"){
			$users = Task::where('company_id',$current_user->company_id)->get();
		}
		else{
			$matchThese = ['company_id' => $current_user->company_id,  'user_id' =>$current_user->id];
			$users = Task::where($matchThese)->get();
		}
		$model_name = "Task";
		return view('admin.get_tasks',compact('users','model_name'));

    }
    public function taskDetails($id){
    	$current_user = Auth::user();
    	$projects = Project::where('company_id',$current_user->company_id)->get();
		$user = Task::find($id);
		$tasks = Task::where('company_id',$current_user->company_id)->get();
		$users = User::where('company_id',$current_user->company_id)->get();
		$dependency_types = Dependency_type::get();
		return view('admin.task_details',compact('user','projects','dependency_types','users','tasks'));

    }

    public function getTaskOrder(Request $request){
    	$project_id = $request['project_id'];
    	$max_order=Task::where('project_id',$project_id)->orderBy('task_order','desc')->first();
    	$tasks=Task::where('project_id',$project_id)->orderBy('task_order','asc')->get();
    	$next_order = $max_order['task_order'] +1;
    	$data = array();
    	$data['order'] = $next_order;
    	$data['tasks'] = $tasks;
    	return json_encode($data);
    }
    public function updateTaskProgress(Request $request){
    	Task::where('id',$request['task_id'])->update(['task_progress' => $request['progress']]);

    }

    public function save(Request $request){
		$data = $request->input();
 		if($data['est_duration_unit'] == "Days" ){
 			$est_duration = $data['est_duration']*1;
 		}
 		if($data['est_duration_unit'] == "Weeks" ){
 			$est_duration = $data['est_duration']*7;
 		}
 		if($data['est_duration_unit'] == "Months" ){
 			$est_duration = $data['est_duration']*30;
 		}
 		/*
 		if($data['act_duration_unit'] == "Days" ){
 			$act_duration = $data['act_duration']*1;
 		}
 		if($data['act_duration_unit'] == "Weeks" ){
 			$act_duration = $data['act_duration']*7;
 		}
 		if($data['act_duration_unit'] == "Months" ){
 			$act_duration = $data['act_duration']*30;
 		}
 		*/
 		$data['est_duration_days'] = $est_duration;
 		//$data['act_duration_days'] = $act_duration;
 		$data['task_progress'] = $data['task_progress'] /100;
    	if($request->input('id') ==""){
    		
			$user = Task::create($data);
			
			return $user->id;

		/*Mail::send('emails.new_task', ['user' => $user , 'data' => $ad_data ], function ($message) use ($user,$subject,$from,$from_name,$to,$to_name) {
            $message->from($from, 'ASIA Academy');
            $message->to($to, $to_name);
			$message->subject($subject);
        });*/
			
		
	
		}else{
			
			$user = Task::find($request->input('id'))->update($data);
			return $request->input('id');
		}
		

    }
    public function get_duration($date1,$date2){
	
		$diff = strtotime($date2) - strtotime($date1);
		$duration = $diff/(60*60*24);
		if ($date1 == $date2){$duration =1;}
		return $duration+1;
			
		
	}
	public function get_holidays_no($startx,$endx,$holidays){
		$current_user = Auth::user();
		$start = new \DateTime($startx);
		$end = new \DateTime($endx);
		// otherwise the  end date is excluded (bug?)
		$end->modify('+1 day');
		
		$interval = $end->diff($start);
		
		// total days
		$days = $interval->days;
		
		// create an iterateable period of date (P1D equates to 1 day)
		$period = new \DatePeriod($start, new \DateInterval('P1D'), $end);
		
		// best stored as array, so you can add more than one
		$company_holidays = Company_holiday::where('company_id',$current_user->company_id)->first();
		$holidays_no = 0;
		foreach($period as $dt) {
			$curr = $dt->format('D');
			$currFullDay = $dt->format('l');
		
			// for the updated question
			if (in_array($dt->format('Y-m-d'), $holidays)) {
			   $days--;
			   $holidays_no++;
			}
		
			// substract if Saturday or Sunday
			if (strpos($company_holidays->week_days, $currFullDay) !== false) {
			
				$days--;
				$holidays_no++;
			}
		}
		return $holidays_no;
	}

	public 	function get_end_date($start_date , $duration,$holidays){
		
		//$duration--;
		$end_datex = 1111111;
		$holidays_no_old = 0;
		for($i=1;$i<1000;$i++){
			$end_date = strtotime($start_date . " +".$duration."days");
			$holidays_no = $this->get_holidays_no($start_date,date("d-m-Y",$end_date),$holidays);
			if($holidays_no== $holidays_no_old){$i=100000;}
					
	$duration = $duration + $holidays_no-$holidays_no_old;
			$holidays_no_old = $holidays_no;
			
			
			
			
			
			
		}
		return date("d-m-Y",$end_date);
		
	}

	public function schedule($project_id){
		// Set start and end dates to null
		Task::where('project_id',$project_id)->update(['est_start_date'=>NULL,'est_end_date'=>NULL]);
		// Get current project
		$project = Project::where('id',$project_id)->first();
		// Alter date format
		$project_start_date = date("d-m-Y",strtotime($project['est_start_date']));
		// select destinct users for this project
		$users = Task::distinct()->select('user_id')->where('project_id',$project_id)->get();
		// loop through the users in the project
		foreach($users as $user){
			// Get holidays of this user with a join 
			$user_holidays = Holiday::join('users_holidays', 'holidays.id', '=', 'users_holidays.holiday_id')->where('users_holidays.user_id', '=', $user->user_id)->get();
			$holidays = array();
			foreach($user_holidays as $user_holiday){
			$holidays[] = 	$user_holiday['start_date'];
			}
			// Get the user's tasks in this project
			$matchThese = ['project_id' => $project_id,  'user_id' => $user->user_id];
			$tasks = Task::where($matchThese)->orderBy('task_order','asc')->get();
			$i =1;
			// loop through the tasks
			foreach($tasks as $task){
				$splitted = 0;
				$interfered = 0;
				// if it's task number 1 then give it the start_date of the project
				if($i==1){
					$start_date = $project_start_date;
				}
				// get the end date depending on the start date and the duration
				$end_date = $this->get_end_date($start_date ,$task->est_duration_days,$holidays);
				// get the duration with the holidays included
				$no_holidays_duration = $this->get_duration($start_date,$end_date) -1;
				
					// Get Postpone tasks
					$interfering_tasks_postpone = $this->interferingTasksPostpone($start_date,$end_date,$user->user_id);
					// Get Split tasks
					$interfering_tasks_split = $this->interferingTasksSplit($start_date,$end_date,$user->user_id);
					
					// interfering tasks Postpone handlings
						if(isset($interfering_tasks_postpone['id'])){
							$start_date = $interfering_tasks_postpone['est_end_date'];
							// get the end date depending on the start date and the duration
							$end_date = $this->get_end_date($start_date ,$task->est_duration_days,$holidays);
							// get the duration with the holidays included
							$no_holidays_duration = $this->get_duration($start_date,$end_date) -1;
							$splitted = 0;
							$interfered = 1;
						}
						// interfering tasks Split handlings
						$interfering_tasks_split = $this->interferingTasksSplit($start_date,$end_date,$user->user_id);
						if(isset($interfering_tasks_split['id'])){
							echo "Split this task ".$task->task_name;
							$end_date = $this->get_end_date($start_date ,$task->est_duration_days,$holidays);
							$no_holidays_duration = $this->get_duration($start_date,$end_date) -1 + $interfering_tasks_split['duration'];
							$end_date =  $this->addDaysToDate($start_date,$no_holidays_duration);
							$splitted = 1;
							$interfered = 1;
						}
					
				
				// update the task with the newly created data
				Task::find($task->id)->update(['est_start_date'=>date("Y-m-d",strtotime($start_date)) , 'est_end_date'=>date("Y-m-d",strtotime($end_date)),'duration'=>$no_holidays_duration,'interfered'=>$interfered,'splitted'=>$splitted]);
				// set the new start date for the next task
				$start_date = strtotime($end_date);
				$start_date = date("d-m-Y",$start_date);
				$i++;
			}
		}

	// Alter dependent tasks
		// get task with dependancies
		$matchThese = ['project_id' => $project_id];
		$dependent_tasks = Task::where($matchThese)->where('dependency_id' ,'!=',0)->get();
		// loop through the tasks
		foreach($dependent_tasks as $task){
			// get the parent tasks
			$parent_task = Task::find($task->dependency_id);
			// compare the parent task end date with the task start_date
			$d1 = new \DateTime($task->est_start_date);
			$d2 = new \DateTime($parent_task->est_end_date);
			// if the task start date is less than its parent end date then alter the dates
			if($d1 < $d2){
				
				$new_est_start_date = $parent_task->est_end_date;
				// get the end date depending on the start date and the duration
				$end_date = $this->get_end_date($new_est_start_date ,$task->est_duration_days,$holidays);
				// get the duration with the holidays included
				$no_holidays_duration = $this->get_duration($new_est_start_date,$end_date) -1;
				//$update = Task::find($task->id)->update(['est_start_date'=>$new_est_start_date,'est_end_date'=>$end_date,]);
				$update = Task::find($task->id)->update(['est_start_date'=>date("Y-m-d",strtotime($new_est_start_date)) , 'est_end_date'=>date("Y-m-d",strtotime($end_date)),'duration'=>$no_holidays_duration]);
			}
		}
		
		
		

		

	}
	public function interferingTasksPostpone($start_date,$end_date,$user_id){

		$start_date = date('Y-m-d H:i:s',strtotime($start_date));
		$end_date = date('Y-m-d H:i:s',strtotime($end_date));
		$interfering_tasks_start = Task::
		where('est_start_date',  $start_date)->
		where('est_end_date',  $end_date)->
		where('user_id',$user_id);

		$interfering_tasks_end = Task::
		where('est_start_date', '<=', $start_date)->
		where('est_end_date', '>=', $end_date)->
		where('user_id',$user_id);

		$interfering_tasks_end1 = Task::
		where('est_start_date', '<', $start_date)->
		where('est_end_date', '<', $end_date)->
		where('est_end_date', '>', $start_date)->
		where('user_id',$user_id);

		$interfering_tasks = $interfering_tasks_start->union($interfering_tasks_end)->union($interfering_tasks_end1)->orderBy('est_end_date','desc')->first();
		return $interfering_tasks;
		
	}
	public function interferingTasksSplit($start_date,$end_date,$user_id){

		$start_date = date('Y-m-d H:i:s',strtotime($start_date));
		$end_date = date('Y-m-d H:i:s',strtotime($end_date));
		

		$query = Task::
		where('est_start_date', '>', $start_date)->
		where('est_end_date', '>', $end_date)->
		where('est_start_date', '<', $end_date)->
		where('user_id',$user_id);

		

		$interfering_tasks = $query->orderBy('est_end_date','desc')->first();
		return $interfering_tasks;
		
	}

	public function addDaysToDate($date,$days){
		return date('Y-m-d H:i:s', strtotime($date. ' + '.$days.' days'));
	}

}
