<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\File;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Storage;
use Input;

class fileController extends Controller
{
	public function upload_files(Request $request)
    {
        $data = $request->input();
        $data['table_name'] = trim($data['table_name']);
    	$originalName = $request->file('file')->getClientOriginalName();
    	$name = $this->renameFile($originalName,'files/'.$data['table_name'].'/'.$data['stuff_id'].'/');

        $data['file'] = $name;

    	
    	$insert_file = File::create($data);
    	$upload = $request->file('file')->move('files/'.$data['table_name'].'/'.$data['stuff_id'].'/' , $name);
    }
    public function getStuffFiles($table_name,$stuff_id)
    {	
    	$result  = array();
    	$file = new File;
    	$files = $file->getStuffFiles($table_name,$stuff_id);

    	foreach ( $files as $file ) {
            if ( '.'!=$file && '..'!=$file) {       //2
                $obj['name'] = $file['file'];
                $obj['size'] = filesize('files/'.$table_name.'/'.$stuff_id.'/'.$file['file']);
                $result[] = $obj;
            }
        }
    	
    return json_encode($result);
    }
    public function deleteFile(Request $request){
        $data = $request->input();
        $data['table_name'] = trim($data['table_name']);
        $matchThese = ['table_name' => $data['table_name'], 'file' => $data['file'], 'stuff_id' => $data['stuff_id']];
        File::where($matchThese)->delete();
        Storage::delete('/files/'.$data['file'].'');

    }
    public function renameFile($name , $path){
	    // ensure a safe filename
	    $name = preg_replace("/[^A-Z0-9._-]/i", "_", $name);
	 
	    // don't overwrite an existing file
	    $i = 0;
	    $parts = pathinfo($name);
	    while (file_exists($path . $name)) {
	        $i++;
	        $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
	    }
		 return $name;
    }
}
