<?php

namespace App\Http\Controllers\Admin;
use App\User;
use Illuminate\Http\Request;
use Hash;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
	public function createOriginalUser(){
		$data =array();
		$data['name'] = 'Amr Sharkas';
		$data['email'] = 'amr@sharkas.net';
		$data['password'] = Hash::make('123456');	
		$user = User::create($data);
	}
	
    public function getUsers(){
    	$current_user = Auth::user();
		$users = User::where('company_id',$current_user->company_id)->get();
		$model_name = "User";
		return view('admin.get_users',compact('users','model_name'));

    }
    public function userDetails($id){

		$user = User::find($id);
		return view('admin.user_details',compact('user'));

    }

    public function save(Request $request){
		$data = $request->input();
 		$data['password'] = Hash::make($data['password']);		
    	if($request->input('id') ==""){
			$user = User::create($data);
		}else{
			$user = User::find($request->input('id'))->update($data);
		}
		return 'Success';

    }
}
