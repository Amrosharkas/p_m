<?php

namespace App\Http\Controllers\admin;
use App\Client;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ClientController extends Controller
{
   public function getClients(){
   		$current_user = Auth::user();
		$users = Client::where('company_id',$current_user->company_id)->get();
		$model_name = "Client";
		return view('admin.get_clients',compact('users','model_name'));

    }
    public function clientDetails($id){

		$user = Client::find($id);
		return view('admin.client_details',compact('user'));

    }

    public function save(Request $request){
		$data = $request->input();
 		dump($data) ;
    	if($request->input('id') ==""){
    		dump($data) ;
			$user = Client::create($data);
		}else{
			dump($data) ;
			$user = Client::find($request->input('id'))->update($data);
		}
		return 'Success';

    }
}
