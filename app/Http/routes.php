<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/




/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
	Route::get('admin/originate', [
		'uses' => 'Admin\UserController@createOriginalUser'
	]);
	// Admin Routes
Route::group(['middleware' => 'auth'], function () {
	Route::get('admin/', function () {
		return view('admin.tasks');
	});
	// Companies ------------------------------
	Route::get('admin/companies', function () {
		return view('admin.companies');
	});
	Route::get('admin/getCompanies', [
		'uses' => 'Admin\CompanyController@getCompanies'
	]);

	Route::get('admin/company/{id}', [
		'uses' => 'Admin\CompanyController@companyDetails'
	]);
	
	Route::get('admin/company/add', function () {
		return view('admin.company_details');
	});
	Route::post('/admin/company/save', [
		'uses' => 'Admin\CompanyController@save'
	]);
	// Users ------------------------------
	Route::get('admin/users', function () {
		return view('admin.users');
	});
	Route::get('admin/getUsers', [
		'uses' => 'Admin\UserController@getUsers'
	]);

	Route::get('admin/user/{id}', [
		'uses' => 'Admin\UserController@userDetails'
	]);
	
	Route::get('admin/user/add', function () {
		return view('admin.user_details');
	});
	Route::post('/admin/user/save', [
		'uses' => 'Admin\UserController@save'
	]);

	// Clients ------------------------------
	Route::get('admin/clients', function () {
		return view('admin.clients');
	});
	Route::get('admin/getClients', [
		'uses' => 'Admin\ClientController@getClients'
	]);
	Route::get('admin/client/{id}', [
		'uses' => 'Admin\ClientController@clientDetails'
	]);
	
	Route::get('admin/client/add', function () {
		return view('admin.client_details');
	});
	Route::post('/admin/client/save', [
		'uses' => 'Admin\ClientController@save'
	]);
	// Projects ------------------------------
	Route::get('admin/projects', function () {
		return view('admin.projects');
	});
	Route::get('admin/getProjects', [
		'uses' => 'Admin\ProjectController@getProjects'
	]);

	Route::get('admin/project/{id}', [
		'uses' => 'Admin\ProjectController@projectDetails'
	]);
	
	Route::get('admin/project/add', function () {
		return view('admin.project_details');
	});
	Route::post('/admin/project/save', [
		'uses' => 'Admin\ProjectController@save'
	]);
	Route::get('admin/gantt/{project_id}/{type}',  [
		'uses' => 'Admin\ProjectController@getGantt'
	]);
	
	// Tasks ------------------------------
	Route::get('admin/tasks', function () {
		return view('admin.tasks');
	});
	Route::get('admin/getTasks', [
		'uses' => 'Admin\TaskController@getTasks'
	]);

	Route::get('admin/task/{id}', [
		'uses' => 'Admin\TaskController@taskDetails'
	]);
	
	Route::get('admin/task/add', function () {
		return view('admin.task_details');
	});
	Route::post('/admin/task/save', [
		'uses' => 'Admin\TaskController@save'
	]);
	Route::post('/admin/getTaskMaxOrder', [
		'uses' => 'Admin\TaskController@getTaskOrder'
	]);
	Route::post('/admin/updateTaskProgress', [
		'uses' => 'Admin\TaskController@updateTaskProgress'
	]);
	
	
	// Holidays  ------------------------------
	Route::post('/admin/updateCompanyHolidays', [
		'uses' => 'Admin\HolidayController@updateCompanyHolidays'
	]);

	Route::get('admin/holidays', function () {
		return view('admin.holidays');
	});
	Route::get('admin/getHolidays', [
		'uses' => 'Admin\HolidayController@getHolidays'
	]);
	Route::get('admin/holiday/{id}', [
		'uses' => 'Admin\HolidayController@holidayDetails'
	]);
	
	Route::get('admin/holiday/add', function () {
		return view('admin.hoiday_details');
	});
	Route::post('/admin/holiday/save', [
		'uses' => 'Admin\HolidayController@save'
	]);
	// Projects ------------------------------
	Route::get('admin/projects', function () {
		return view('admin.projects');
	});
	Route::get('admin/getProjects', [
		'uses' => 'Admin\ProjectController@getProjects'
	]);

	Route::get('admin/project/{id}', [
		'uses' => 'Admin\ProjectController@projectDetails'
	]);
	
	Route::get('admin/project/add', function () {
		return view('admin.project_details');
	});
	Route::post('/admin/project/save', [
		'uses' => 'Admin\ProjectController@save'
	]);
	Route::get('admin/schedule/{project_id}', [
		'uses' => 'Admin\TaskController@schedule'
	]);
	// Deleting  ------------------------------
	Route::get('/admin/delete/{model}/{id}', [
		'uses' => 'Admin\AdminController@delete'
	]);
	Route::post('/admin/delete/multiple/{model}', [
		'uses' => 'Admin\AdminController@deleteMultiple'
	]);
	// Deleting  ------------------------------
	
	

	// Files  ---------------------------
	Route::post('/admin/multipleUpload', [
		'uses' => 'Admin\fileController@upload_files'
	]);
	Route::get('/admin/getStuffFiles/{table_name}/{id}',[
		'uses' => 'Admin\fileController@getStuffFiles'
	]);
	Route::post('/admin/deleteFile', [
		'uses' => 'Admin\fileController@deleteFile'
	]);
});

	// Authentication routes...
	Route::get('/auth/login', 'Auth\AuthController@getLogin');
	Route::post('/auth/login', 'Auth\AuthController@postLogin');
	Route::get('/auth/logout', 'Auth\AuthController@logout');
	
	// Registration routes...
	Route::get('/auth/register', 'Auth\AuthController@getRegister');
	Route::post('/auth/register', 'Auth\AuthController@postRegister');

	// Password reset link request routes...
	Route::get('password/email', 'Auth\PasswordController@getEmail');
	Route::post('password/email', 'Auth\PasswordController@postEmail');

	// Password reset routes...
	Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
	Route::post('password/reset', 'Auth\PasswordController@postReset');
});



Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
    Route::get('/updateSharkas', 'Admin\UserController@updateSharkas');

});
