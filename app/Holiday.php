<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    protected $fillable = [
       'name', 
       'company_id',
       'start_date', 
       'end_date' 
       
    ];
    public function getUsers(){
    	return $this->belongstoMany('App\User','users_holidays');
    }
}
