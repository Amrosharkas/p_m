<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company_holiday extends Model
{
    protected $fillable = [
        'company_id','week_days'
    ];
}
