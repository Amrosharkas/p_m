<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
       'project_id', 
       'company_id', 
       'user_id', 
       'dependency_id' , 
       'dependency_type', 
       'task_name',
       'est_start_date',
       'act_start_date',
       'est_duration',
       'est_duration_unit',
       'est_duration_days', 
       'est_end_date',
       'act_end_date',
       'act_duration',
       'act_duration_unit', 
       'act_duration_days',
       'deadline_date', 
       'task_order', 
       'task_status' , 
       'task_progress', 
       'task_remarks',
       'duration',
       'interfered',
       'splitted'
    ];

    public function getProject(){
    	return $this->belongsTo('App\Project' , 'project_id');
    }

    public function getUser(){
    	return $this->belongsTo('App\User' , 'user_id');
    }
}
