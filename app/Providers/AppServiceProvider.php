<?php

namespace App\Providers;
use Auth;
use App\Company;
use App\Company_holiday;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       
            \View::composer('*', function($view){
             if (Auth::user()) { 
                $company_holidays = Company_holiday::where('company_id',Auth::user()->company_id)->first();
                $view->with('company_holidays', $company_holidays);
                $company = Company::where('id',Auth::user()->company_id)->first();
                $view->with('company', $company);
            }
            $view->with('current_user', \Auth::user());
          
             });
        
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
