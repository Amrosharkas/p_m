<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = "projects";
    protected $fillable = [
       'client_id', 'company_id', 'project_name','domain_name','old_url','project_logo','est_start_date','act_start_date','est_end_date','act_end_date','deadline_date','project_remarks'
    ];
}
