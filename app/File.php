<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table ='multiple_files';
    protected $fillable = [
        'stuff_id','table_name','file'
    ];
    public function getStuffFiles($table_name,$stuff_id){
    	$matchThese = ['table_name' => $table_name,  'stuff_id' => $stuff_id];
    	return $this->where($matchThese)->get();
    }
}
