<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_holiday extends Model
{
	protected $table = 'users_holidays';
    protected $fillable = [
       'company_id', 
       'user_id', 
       'holiday_id'
    ];

}
