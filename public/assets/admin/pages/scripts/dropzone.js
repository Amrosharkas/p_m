var dropzoneAPI = function () {

    var handleDropzone = function () {

        var stuff_id = $("#id").val();
        var table_name = $('#hidden_attributes').attr('data-table_name');
        var token = $('#hidden_attributes').attr('data-token');
        $('#table_name').val(table_name);
        
        var myDropzone = new Dropzone("#my-dropzone", {
            url: "/admin/multipleUpload",
            addRemoveLinks: "x",
            parallelUploads: 1,
            init: function () {
                thisDropzone = this;
                <!-- 4 -->
                $.get('getStuffFiles/'+table_name+'/' + stuff_id + '', 'json', function (data) {
                    var stuffFiles = jQuery.parseJSON(data);
                    <!-- 5 -->
                    $.each(stuffFiles, function (key, value) {
                        var mockFile = {
                            name: value.name,
                            size: value.size
                        };

                        thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                        var fileExtension = value.name.substr((value.name.lastIndexOf('.') + 1));
                        if (fileExtension == "mp3") {
                            thisDropzone.options.thumbnail.call(thisDropzone, mockFile, "/files/icons/audio.jpg");
                        } else {
                            thisDropzone.options.thumbnail.call(thisDropzone, mockFile, "/files/"+table_name+"/" + stuff_id + "/" + value.name);
                        }


                    });

                });
            }
        });
        myDropzone.on("removedfile", function (file) {

            $.ajax(

                {
                    url: "/admin/deleteFile",
                    type: "POST",
                    data: {
                        file: file.name,
                        table_name: table_name,
                        stuff_id: stuff_id,
                        _token:token
                    },
                    success: function (result) {
                        $("#div1").html(result);
                    }
                });
            /* Maybe display some more file information on your page */
        });




    }

    

    return {

        //main function to initiate the module
        init: function () {
            handleDropzone();
        }
    };

}();
