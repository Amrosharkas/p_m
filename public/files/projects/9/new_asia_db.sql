-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2016 at 11:48 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `new_asia_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_login_tbl`
--

CREATE TABLE IF NOT EXISTS `admin_login_tbl` (
  `ID` int(50) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `user_level` text CHARACTER SET utf8,
  `admin_seen` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `admin_login_tbl`
--

INSERT INTO `admin_login_tbl` (`ID`, `username`, `password`, `user_level`, `admin_seen`) VALUES
(1, 'heba', 'hebaP@ssw0rd', 'Site Settings,Trainers Models,Trainers Requirements,Slider,Users', 1),
(2, 'agent', 'agent123', '', 1),
(3, 'admin1', 'admin123', '1,2', 1),
(4, 'admin123', 'admin123', 'الفيديوهات,كتيبة وشوشة', 1),
(5, 'gogo', 'gogo123', 'الفيديوهات,المناسبات', 1),
(6, 'admin1', 'admin123', NULL, 0),
(7, 'admin1', 'admin123', NULL, 0),
(8, 'admin1', 'admin123', NULL, 0),
(9, 'hamada', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `approval_types`
--

CREATE TABLE IF NOT EXISTS `approval_types` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `approval_types`
--

INSERT INTO `approval_types` (`ID`, `name`) VALUES
(1, 'معلق'),
(2, 'مقبول'),
(3, 'مرفوض');

-- --------------------------------------------------------

--
-- Table structure for table `cats_tbl`
--

CREATE TABLE IF NOT EXISTS `cats_tbl` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL DEFAULT '0',
  `section_id` int(11) DEFAULT NULL,
  `cat_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cats_tbl`
--

INSERT INTO `cats_tbl` (`ID`, `cat_id`, `section_id`, `cat_name`) VALUES
(1, 0, 4, 'Haw');

-- --------------------------------------------------------

--
-- Table structure for table `coop_types_tbl`
--

CREATE TABLE IF NOT EXISTS `coop_types_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `coop_types_tbl`
--

INSERT INTO `coop_types_tbl` (`id`, `name`) VALUES
(1, 'تدريب\r\n'),
(2, 'إستشارات'),
(3, 'تطوير المضامين وإعداد المواد التدريبية');

-- --------------------------------------------------------

--
-- Table structure for table `homepage_photos_tbl`
--

CREATE TABLE IF NOT EXISTS `homepage_photos_tbl` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `new_id` int(11) DEFAULT NULL,
  `new_photo` varchar(255) DEFAULT NULL,
  `new_desc` text CHARACTER SET utf8,
  `new_main` int(11) DEFAULT '0',
  `photo_date` datetime DEFAULT NULL,
  `photos_count` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `insert_photos_tbl`
--

CREATE TABLE IF NOT EXISTS `insert_photos_tbl` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `story_id` int(11) DEFAULT NULL,
  `story_photo` text,
  `story_desc` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `insert_photos_tbl`
--

INSERT INTO `insert_photos_tbl` (`ID`, `story_id`, `story_photo`, `story_desc`) VALUES
(1, 1, 'facegfx-vector-school-design-elements-background-vector-01.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_id` int(11) DEFAULT NULL,
  `to_id` int(11) DEFAULT NULL,
  `seen_status` int(11) NOT NULL DEFAULT '0',
  `message` text,
  `msg_time` datetime DEFAULT NULL,
  `thread_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `send_email` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `from_id`, `to_id`, `seen_status`, `message`, `msg_time`, `thread_id`, `created_at`, `updated_at`, `send_email`) VALUES
(4, 0, 2, 1, 'hellooooo', '2016-02-03 17:01:15', 1, NULL, '2016-02-03 15:01:32', 1),
(5, 2, 0, 0, 'hrloooo', '2016-02-03 15:21:38', 1, '2016-02-03 15:21:39', '2016-02-03 15:21:39', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news_photos_tbl`
--

CREATE TABLE IF NOT EXISTS `news_photos_tbl` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `new_id` int(11) DEFAULT NULL,
  `new_photo` varchar(255) DEFAULT NULL,
  `new_desc` text CHARACTER SET utf8,
  `new_main` int(11) DEFAULT '0',
  `photo_date` datetime DEFAULT NULL,
  `photos_count` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `news_photos_tbl`
--

INSERT INTO `news_photos_tbl` (`ID`, `new_id`, `new_photo`, `new_desc`, `new_main`, `photo_date`, `photos_count`) VALUES
(1, 16, '12041855_10154223069018294_738658530_n.jpg', '', 0, NULL, 1),
(2, 13, 'AQ-Project.docx', '', 0, NULL, 1),
(3, 16, 'mavericks_db.sql', '', 0, NULL, 1),
(4, 13, 'Amr Sharkas.pdf', '', 0, NULL, 1),
(5, 16, '????? ????? ????? (1).xls', '', 0, NULL, 1),
(6, 13, '????? ???? ????? (1).docx', '', 0, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `news_tbl`
--

CREATE TABLE IF NOT EXISTS `news_tbl` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `new_title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `facebook_title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `facebook_brief` text CHARACTER SET utf8,
  `new_date` date DEFAULT NULL,
  `new_body` text CHARACTER SET utf8,
  `new_video` text,
  `new_views` int(11) NOT NULL DEFAULT '0',
  `new_status` int(11) DEFAULT NULL,
  `new_top` int(11) DEFAULT NULL,
  `admin_status` int(11) NOT NULL DEFAULT '0',
  `new_main_photo` text,
  `top_homepage` int(11) NOT NULL DEFAULT '0',
  `new_brief` text CHARACTER SET utf8,
  `news_order` int(11) DEFAULT NULL,
  `meta_desc` text CHARACTER SET utf8,
  `new_tags` text CHARACTER SET utf8 NOT NULL,
  `section_id` int(11) NOT NULL,
  `stuff_user` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `telephone` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `mobile` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `first_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `editor_brief` text CHARACTER SET utf8,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `time_stuff` datetime DEFAULT NULL,
  `faq_question` text CHARACTER SET utf8,
  `faq_answer` text CHARACTER SET utf8,
  `username` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `category` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sub_category` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `stuff_notify` int(11) DEFAULT NULL,
  `notify_title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `notify_msg` text CHARACTER SET utf8,
  `notify_url` text CHARACTER SET utf8,
  `tracking_code` text,
  `android_api_key` text,
  `android_sender_id` text,
  `facebook_url` text,
  `twitter_url` text,
  `instagram_url` text,
  `google_url` text,
  `linkedin_url` text,
  `pinterest_url` text,
  `tumblr_url` text,
  `vine_url` text,
  `flickr_url` text,
  `vk_url` text,
  `site_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `site_language` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `news_tbl`
--

INSERT INTO `news_tbl` (`ID`, `new_title`, `facebook_title`, `facebook_brief`, `new_date`, `new_body`, `new_video`, `new_views`, `new_status`, `new_top`, `admin_status`, `new_main_photo`, `top_homepage`, `new_brief`, `news_order`, `meta_desc`, `new_tags`, `section_id`, `stuff_user`, `serial_no`, `email`, `telephone`, `mobile`, `first_name`, `last_name`, `editor_brief`, `start_time`, `end_time`, `start_date`, `end_date`, `time_stuff`, `faq_question`, `faq_answer`, `username`, `category`, `sub_category`, `stuff_notify`, `notify_title`, `notify_msg`, `notify_url`, `tracking_code`, `android_api_key`, `android_sender_id`, `facebook_url`, `twitter_url`, `instagram_url`, `google_url`, `linkedin_url`, `pinterest_url`, `tumblr_url`, `vine_url`, `flickr_url`, `vk_url`, `site_name`, `site_language`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, NULL, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 01:00:00', '1970-01-01 01:00:00', '1970-01-01', '1970-01-01', '1970-01-01 01:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'haw1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ASIA Academy', 'Ar'),
(5, 'Website SEO', NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, 1, NULL, 0, NULL, NULL, 'Haw', 'asd', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 02:00:00', '1970-01-01 02:00:00', '1970-01-01', '1970-01-01', '1970-01-01 02:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '1', NULL, NULL, '1970-01-01', NULL, NULL, 0, 1, 1, 1, 'doctora-1.png', 1, NULL, 1, NULL, 'dasdas,dasdasd,adasdsa', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 02:00:00', '1970-01-01 02:00:00', '1970-01-01', '1970-01-01', '1970-01-01 02:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, '', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, '', 4, NULL, 'O381TI7q', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '7amada', NULL, NULL, '1970-01-01', NULL, NULL, 0, 0, 0, 1, NULL, 0, NULL, NULL, NULL, '', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 01:00:00', '1970-01-01 01:00:00', '1970-01-01', '1970-01-01', '1970-01-01 01:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, '', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, '', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, '', 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'السيرة الذاتية', NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, 1, '????? ???? ????? (1).docx', 1, NULL, NULL, NULL, '', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 01:00:00', '1970-01-01 01:00:00', '1970-01-01', '1970-01-01', '1970-01-01 01:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'صورة شخصية', NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, 1, NULL, 1, NULL, NULL, NULL, '', 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 01:00:00', '1970-01-01 01:00:00', '1970-01-01', '1970-01-01', '1970-01-01 01:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'صورة جواز السفر', NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, 1, NULL, 1, NULL, NULL, NULL, '', 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 01:00:00', '1970-01-01 01:00:00', '1970-01-01', '1970-01-01', '1970-01-01 01:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'نموذج تعارف', NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, 1, '????? ????? ????? (1).xls', 1, NULL, NULL, NULL, '', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 01:00:00', '1970-01-01 01:00:00', '1970-01-01', '1970-01-01', '1970-01-01 01:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reg_models_tbl`
--

CREATE TABLE IF NOT EXISTS `reg_models_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_name` varchar(255) DEFAULT NULL,
  `model_file` text,
  `validation_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reg_rquirements`
--

CREATE TABLE IF NOT EXISTS `reg_rquirements` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `req_name` varchar(255) DEFAULT NULL,
  `validation_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `site_sections_tbl`
--

CREATE TABLE IF NOT EXISTS `site_sections_tbl` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `new_title` int(11) NOT NULL DEFAULT '1',
  `facebook_title` int(11) NOT NULL DEFAULT '1',
  `facebook_brief` int(11) NOT NULL DEFAULT '1',
  `new_date` int(11) NOT NULL DEFAULT '1',
  `new_body` int(11) NOT NULL DEFAULT '1',
  `new_video` int(11) NOT NULL DEFAULT '1',
  `new_views` int(11) NOT NULL DEFAULT '1',
  `new_status` int(11) NOT NULL DEFAULT '1',
  `new_top` int(11) NOT NULL DEFAULT '1',
  `admin_status` int(11) NOT NULL DEFAULT '1',
  `new_main_photo` int(11) NOT NULL DEFAULT '1',
  `top_homepage` int(11) NOT NULL DEFAULT '1',
  `new_brief` int(11) NOT NULL DEFAULT '1',
  `news_order` int(11) NOT NULL DEFAULT '1',
  `meta_desc` int(11) NOT NULL DEFAULT '1',
  `new_tags` int(11) NOT NULL DEFAULT '1',
  `section_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `admin_seen` int(11) NOT NULL DEFAULT '0',
  `stuff_user` int(11) NOT NULL DEFAULT '1',
  `serial_no` int(11) NOT NULL DEFAULT '1',
  `video_gallery` int(11) NOT NULL DEFAULT '1',
  `email` int(11) NOT NULL DEFAULT '1',
  `telephone` int(11) NOT NULL DEFAULT '1',
  `mobile` int(11) NOT NULL DEFAULT '1',
  `first_name` int(11) NOT NULL DEFAULT '1',
  `last_name` int(11) NOT NULL DEFAULT '1',
  `editor_brief` int(11) NOT NULL DEFAULT '1',
  `start_time` int(11) NOT NULL DEFAULT '1',
  `end_time` int(11) NOT NULL DEFAULT '1',
  `start_date` int(11) NOT NULL DEFAULT '1',
  `end_date` int(11) NOT NULL DEFAULT '1',
  `time_stuff` int(11) NOT NULL DEFAULT '1',
  `faq_question` int(11) NOT NULL DEFAULT '1',
  `faq_answer` int(11) NOT NULL DEFAULT '1',
  `username` int(11) NOT NULL DEFAULT '1',
  `category` int(11) NOT NULL DEFAULT '0',
  `sub_category` int(11) NOT NULL DEFAULT '0',
  `stuff_notify` int(11) NOT NULL DEFAULT '0',
  `notify_title` int(11) NOT NULL DEFAULT '0',
  `notify_msg` int(11) NOT NULL DEFAULT '0',
  `notify_url` int(11) NOT NULL DEFAULT '0',
  `tracking_code` int(11) NOT NULL DEFAULT '0',
  `android_api_key` int(11) NOT NULL DEFAULT '0',
  `android_sender_id` int(11) NOT NULL DEFAULT '0',
  `facebook_url` int(11) NOT NULL DEFAULT '0',
  `twitter_url` int(11) NOT NULL DEFAULT '0',
  `instagram_url` int(11) NOT NULL DEFAULT '0',
  `google_url` int(11) NOT NULL DEFAULT '0',
  `linkedin_url` int(11) NOT NULL DEFAULT '0',
  `tumblr_url` int(11) NOT NULL DEFAULT '0',
  `vine_url` int(11) NOT NULL DEFAULT '0',
  `flickr_url` int(11) NOT NULL DEFAULT '0',
  `vk_url` int(11) NOT NULL DEFAULT '0',
  `site_name` int(11) NOT NULL DEFAULT '0',
  `site_language` int(11) NOT NULL DEFAULT '0',
  `pinterest_url` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `site_sections_tbl`
--

INSERT INTO `site_sections_tbl` (`ID`, `new_title`, `facebook_title`, `facebook_brief`, `new_date`, `new_body`, `new_video`, `new_views`, `new_status`, `new_top`, `admin_status`, `new_main_photo`, `top_homepage`, `new_brief`, `news_order`, `meta_desc`, `new_tags`, `section_name`, `admin_seen`, `stuff_user`, `serial_no`, `video_gallery`, `email`, `telephone`, `mobile`, `first_name`, `last_name`, `editor_brief`, `start_time`, `end_time`, `start_date`, `end_date`, `time_stuff`, `faq_question`, `faq_answer`, `username`, `category`, `sub_category`, `stuff_notify`, `notify_title`, `notify_msg`, `notify_url`, `tracking_code`, `android_api_key`, `android_sender_id`, `facebook_url`, `twitter_url`, `instagram_url`, `google_url`, `linkedin_url`, `tumblr_url`, `vine_url`, `flickr_url`, `vk_url`, `site_name`, `site_language`, `pinterest_url`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '', 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Site Settings', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 'Website SEO', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '', 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 'Trainers Models', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(7, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'Trainers Requirements', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `threads`
--

CREATE TABLE IF NOT EXISTS `threads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user2_id` int(11) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `threads`
--

INSERT INTO `threads` (`id`, `user_id`, `user2_id`, `last_update`, `created_at`, `updated_at`) VALUES
(1, 2, 0, '2016-02-03 15:21:40', '2016-02-03 14:56:24', '2016-02-03 15:21:40');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `name` text,
  `telephone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `speciality` text,
  `coop_type_id` text,
  `cv_user` text,
  `approved_user` int(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `current_step` int(255) DEFAULT '1',
  `user_type` int(11) DEFAULT NULL,
  `membership_number` varchar(255) DEFAULT NULL,
  `address` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `name`, `telephone`, `email`, `password`, `nationality`, `speciality`, `coop_type_id`, `cv_user`, `approved_user`, `created_at`, `updated_at`, `remember_token`, `current_step`, `user_type`, `membership_number`, `address`) VALUES
(1, 'Amr', '', 'sharkas', NULL, '+201112421335', 'Amrosharkas@gmail.com', '$2y$10$5JqYbuhlEQ1dbmEjzFvreeLkD.6vkbjUG.2495ForpovR5YhbXk8m', 'EG', 'Web development', ', تدريب\r\n, إستشارات', 'Amrosharkas@gmail.com.xls', 1, '2016-02-03 14:55:29', '2016-02-03 14:55:29', NULL, 2, 1, NULL, 'Alexandria egypt'),
(2, 'Amr', '', 'sharkas', NULL, '+201112421335', 'Amrosharkas@gmail.com', '$2y$10$Rxz8Z.avaN4k4OYPdVnzPecFDFzq0OFqYl5unEMmlStA6Tlem1sfS', 'EG', 'Web development', ', تدريب\r\n, إستشارات', 'Amrosharkas@gmail.com.xls', 1, '2016-02-03 14:56:02', '2016-02-03 14:56:02', NULL, 2, 1, NULL, 'Alexandria egypt');

-- --------------------------------------------------------

--
-- Table structure for table `users_valitaions`
--

CREATE TABLE IF NOT EXISTS `users_valitaions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `req_id` int(11) DEFAULT NULL,
  `approved` int(11) DEFAULT NULL,
  `file_name` text,
  `req_type` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `admin_seen` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `users_valitaions`
--

INSERT INTO `users_valitaions` (`ID`, `user_id`, `req_id`, `approved`, `file_name`, `req_type`, `updated_at`, `created_at`, `admin_seen`) VALUES
(1, 1, 13, 1, NULL, 1, '2016-02-02 20:33:23', '2016-02-02 20:33:23', 0),
(2, 1, 16, 1, NULL, 1, '2016-02-02 20:33:23', '2016-02-02 20:33:23', 0),
(3, 1, 14, 1, NULL, 2, '2016-02-02 20:33:23', '2016-02-02 20:33:23', 0),
(4, 1, 15, 1, NULL, 2, '2016-02-02 20:33:23', '2016-02-02 20:33:23', 0),
(5, 2, 13, 1, NULL, 1, '2016-02-03 14:56:09', '2016-02-03 14:56:09', 0),
(6, 2, 16, 1, NULL, 1, '2016-02-03 14:56:09', '2016-02-03 14:56:09', 0),
(7, 2, 14, 1, NULL, 2, '2016-02-03 14:56:10', '2016-02-03 14:56:10', 0),
(8, 2, 15, 1, NULL, 2, '2016-02-03 14:56:10', '2016-02-03 14:56:10', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
