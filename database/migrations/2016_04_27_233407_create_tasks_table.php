<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->nullable();
            $table->integer('project_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('task_name')->nullable();
            $table->integer('dependency_id')->nullable();
            $table->integer('dependency_type')->nullable();
            $table->string('task_type')->nullable();
            $table->datetime('est_start_date')->nullable();
            $table->datetime('est_end_date')->nullable();
            $table->integer('est_duration')->nullable();
            $table->string('est_duration_unit')->nullable();
            $table->integer('est_duration_days')->nullable();
            $table->datetime('act_start_date')->nullable();
            $table->datetime('act_end_date')->nullable();
            $table->integer('act_duration')->nullable();
            $table->string('act_duration_unit')->nullable();
            $table->integer('act_duration_days')->nullable();
            $table->integer('duration')->nullable();
            $table->datetime('deadline_date')->nullable();
            $table->integer('task_status')->nullable();
            $table->decimal('task_progress')->default(0);
            $table->integer('task_order')->default(0);
            $table->text('task_remarks')->nullable();
            $table->integer('interfered')->default(0);
            $table->integer('splitted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tasks');
    }
}
