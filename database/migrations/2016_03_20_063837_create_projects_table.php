<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->nullable();
            $table->integer('client_id')->nullable();
            $table->string('project_name')->nullable();
            $table->string('domain_name')->nullable();
            $table->string('old_url')->nullable();
            $table->string('project_logo')->nullable();
            $table->date('est_start_date')->nullable();
            $table->date('act_start_date')->nullable();
            $table->date('est_end_date')->nullable();
            $table->date('act_end_date')->nullable();
            $table->date('deadline_date')->nullable();
            $table->string('project_remarks')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
