
<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i></div>
							<div class="tools">
								<a href="javascript:;" class="collapse" data-original-title="" title="">
								</a>
							
							</div>
						</div>
						<div class="portlet-body form">
							
							

		<form action="/admin/task/save" method="post" class="j-forms" id="j-forms" novalidate>
        <input type="hidden" name="company_id" id="company_id" value="{{$current_user->company_id}}"/>
 <input type="hidden" name="id" id="id" value="@if(isset($user)){{$user->id}}@endif" />
		{!! csrf_field() !!}

			<div class="content">
				<!-- start Client -->
				  <div class="j-row">
				<div class="unit">
                
					<label class="input select">
						<select name="project_id" id="project_id" data-placeholder="select a project"  class="chosen-select" >
							<option value=""></option>
                            @foreach($projects as $project)
							<option @if(isset($user))  @if( $user->project_id == $project->id) selected  @endif @endif value="{{$project->id}}">{{$project->project_name}}</option>
                            @endforeach
						</select>
					</label>
				</div>
            </div>
				<!-- end Client -->
                <!-- start User -->
				  <div class="j-row">
				<div class="unit">
					<label class="input select">
						<select name="user_id" data-placeholder="Assigned to"  class="chosen-select">
							<option value=""></option>
                            @foreach($users as $userx)
							<option @if(isset($user))  @if( $user->user_id == $userx->id) selected  @endif @endif value="{{$userx->id}}">{{$userx->name}}</option>
                            @endforeach
						</select>
					</label>
				</div>
            </div>
				<!-- end User -->
              
				<!-- start name -->
				<div class="j-row">
					
					<div class="span6 unit">
                    <label class="label">Name</label>
						<div class="input">
							<label class="icon-right" for="name">
								<i class="fa fa-globe"></i>
							</label>
							<input type="text" id="name" name="task_name" @if(isset($user)) value="{{$user->task_name}}" @endif>
						</div>
					</div>
                    <div class="span6 unit" id="dependency_id">
                    <label class="label">Dependending On</label>
                    <label class="input select">
						<select name="dependency_id" data-placeholder="Depending on"  class="chosen-select dependency_id">
							<option value=""></option>
                            @if(isset($user))
                            @foreach($tasks as $task)
							<option @if(isset($user))  @if( $user->dependency_id == $task->id) selected  @endif @endif value="{{$task->id}}">{{$task->task_name}}</option>
                            @endforeach
                            @endif
						</select>
					</label>
                    <input type="hidden" name="dependency_type" value="1" />
					</div>
				</div>
				<!-- end name -->

				<!-- start Estimated Dates -->
				<div class="j-row">
					<div class="span4 unit">
						<label class="label">Estimated start date</label>
						<div class="input">
							<label class="icon-right" for="date_from">
								<i class="fa fa-calendar"></i>
							</label>
							<input type="text" id="est_start_date" name="est_start_date" class="datetimePicker" @if(isset($user)) value="{{$user->est_start_date}}" @endif>
						</div>
					</div>
					<div class="span4 unit">
						<label class="label">Estimated end date</label>
						<div class="input">
							<label class="icon-right" for="date_to">
								<i class="fa fa-calendar"></i>
							</label>
							<input type="text" id="est_end_date" name="est_end_date" class="datetimePicker" @if(isset($user)) value="{{$user->est_end_date}}" @endif>
						</div>
					</div>
                    <div class="span2 unit">
						<label class="label">Estimated Duration</label>
						<div class="input">
							<label class="icon-right" for="date_from">
								<i class="fa fa-clock-o"></i>
							</label>
							<input type="text" id="est_duration" name="est_duration"  @if(isset($user)) value="{{$user->est_duration}}" @endif>
						</div>
					</div>
                    <div class="span2 unit">
						<label class="label">Unit</label>
						<div class="input">
                         <label class="input select">
							<select name="est_duration_unit">
							
                            
							<option @if(isset($user))  @if( $user->est_duration_unit == "Days") selected  @endif @endif value="Days">Days</option>
                            <option @if(isset($user))  @if( $user->est_duration_unit == "Weeks") selected  @endif @endif value="Weeks">Weeks</option>
                            <option @if(isset($user))  @if( $user->est_duration_unit == "Months") selected  @endif @endif value="Months">Months</option>
                            
						</select>
                       
						
						<i></i>
					</label>
						</div>
					</div>
				</div>
				<!-- end Estimated Dates -->
				<!-- start Actual Dates -->
				<div class="j-row">
					<div class="span5 unit">
						<label class="label">Actual start date</label>
						<div class="input">
							<label class="icon-right" for="date_from">
								<i class="fa fa-calendar"></i>
							</label>
							<input type="text" id="act_start_date" name="act_start_date" class="datetimePicker" @if(isset($user)) value="{{$user->act_start_date}}" @endif> 
						</div>
					</div>
					<div class="span5 unit">
						<label class="label">Actual end date</label>
						<div class="input">
							<label class="icon-right" for="date_to">
								<i class="fa fa-calendar"></i>
							</label>
							<input type="text" id="act_end_date" name="act_end_date" class="datetimePicker" @if(isset($user)) value="{{$user->act_end_date}}" @endif>
						</div>
					</div>
                    <div class="span2 unit">
						<label class="label">Actual Duration</label>
						<div class="input" style="display:none;">
							
								<strong>3 Weeks</strong>
							
							
						</div>
					</div>
                    
				</div>
				<!-- end Actual Dates -->
                <!-- start Deadline -->
				<div class="j-row">
					
					<div class="span6 unit">
						<label class="label">Deadline date</label>
						<div class="input">
							<label class="icon-right" for="date_to">
								<i class="fa fa-calendar"></i>
							</label>
							<input type="text" id="deadline_date" name="deadline_date" class="datePicker" @if(isset($user)) value="{{$user->deadline_date}}" @endif>
						</div>
					</div>
                    <div class="span3 unit">
					<div class="slider-group error-view">
						Progress:
						<label id="1-h"></label>
					</div>
					<div id="slider-1-h"></div>
                    <input type="hidden" id="task_progress" name="task_progress" @if(isset($user)) value="{{$user->task_progress*100}}" @endif />
					</div>
                    <div class="span3 unit">
						<label class="label">Order</label>
						<div class="input">
							<label class="icon-right" for="date_to">
								<i class="fa fa-check"></i>
							</label>
							<input type="text" id="task_order" name="task_order"  @if(isset($user)) value="{{$user->task_order}}" @endif>
						</div>
					</div>
				</div>
				<!-- end Deadline-->
                <!-- start Remarks -->
				<div class="j-row">
					
					<div class="span12 unit">
						<label class="label">Remarks</label>
						<div class="input">
							                <textarea placeholder="Remarks" spellcheck="true" name="task_remarks">@if(isset($user)) {{$user->task_remarks}} @endif</textarea>

						</div>
					</div>
				</div>
				<!-- end Remarks-->


				<!-- start response from server -->
				<div id="response" style="display:none;"></div>
				<!-- end response from server -->

			</div>
			<!-- end /.content -->
			<div class="footer">
				<button type="submit" class="primary-btn" id="enable-button" >Submit</button>
			</div>
			<!-- end /.footer -->

		</form>
        
        
	
						</div>
						<!-- END VALIDATION STATES-->
					</div>
					<div class="portlet box blue" @if(!isset($user)) style="display:none;" @endif id="multipleUpload">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-file"></i>&nbsp; Task Files</div>
							<div class="tools">
								<a href="javascript:;" class="collapse" data-original-title="" title="">
								</a>
							
							</div>
						</div>
                        <div class="portlet-body form">
                        <form action="/admin/multipleUpload" class="dropzone" id="my-dropzone"   enctype="multipart/form-data">
                        {!! csrf_field() !!}
                         <input type="hidden" name="stuff_id" id="stuff_id" value="@if(isset($user)){{$user->id}}@endif" />
                         <input type="hidden" name="table_name" value="" id="table_name" />

                        </form>
                        </div>
                        
                        </div>
                        
             