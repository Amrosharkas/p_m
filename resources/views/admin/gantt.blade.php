@extends('admin.master')
<?php $i=2; $j =1;?>
@section('add_css')
    <link rel="stylesheet" href="/assets/global/plugins/gantt/dhtmlxgantt.css" type="text/css" media="screen" title="no title" charset="utf-8">

@stop

@section('add_js_plugins')
    <script src="/assets/global/plugins/gantt/dhtmlxgantt.js" type="text/javascript" charset="utf-8"></script>
    <script src="/assets/global/plugins/gantt/ext/dhtmlxgantt_marker.js" type="text/javascript" charset="utf-8"></script>
    <script src="/assets/global/plugins/gantt/ext/dhtmlxgantt_tooltip.js" type="text/javascript" charset="utf-8"></script>
    <script src="/assets/global/plugins/gantt/ext/dhtmlxgantt_fullscreen.js" type="text/javascript" charset="utf-8"></script>
    <script src="http://export.dhtmlx.com/gantt/api.js" type="text/javascript" charset="utf-8"></script>
@stop

@section('add_js_scripts')
	<script type="text/javascript">
	var tasks =  {
		data:[
				@foreach($tasks as $task)
				<?php 
					if($type == "Estimated"){
						$start_date = $task->est_start_date;
						$end_date = $task->est_end_date;
						$duration = $task->duration;
						
					}else{
						$start_date = $task->act_start_date;
						$end_date = $task->act_end_date;
						$duration = Null;
					}
				?>
			{id:{{$task->id}}, 
			text:"{{$task->task_name}}", 
			start_date:"{{$start_date}}", 
			end_date:"{{$end_date}}",
			order:{{$task->task_order}},
			priority: <?php if($task->user_id==16){?>"1"<?php ;}if($task->user_id==19){?>"4"<?php ;}?><?php if($task->user_id==22){?>"2"<?php ;}?>, 
			color : "{{$task->getUser->user_color}}",
			username:"{{$task->getUser->name}}",
				 open: true},
				@endforeach
			
		],
		links:[
			   <?php $l =1;?>
			@foreach($links as $link)
				{ id:{{$l}}, source:{{$link->dependency_id}}, target:{{$link->id}}, type:"0"},
				<?php $l++;?>
			@endforeach
			
		]
	};
	</script>
	<script src="/assets/admin/pages/scripts/ganttInit.js"></script>
@stop

@section('add_inits')
	GanttInit.init();
@stop

@section('title')
	{{$type}} {{$project->project_name}}  Plan
@stop

@section('page_title')
{{$project->project_name}} <button  class="btn blue pull-right" type="button" onclick='gantt.exportToPNG()' ><i class="fa fa-arrow-left"></i> Export </button>
@stop

@section('page_title_small')
	
@stop
@section('body_attributes')
	
@stop
@section('content')
	
	<div id="gantt_here" style='width:100%; height:100%;'></div>

@stop