<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i></div>
							<div class="tools">
								<a href="javascript:;" class="collapse" data-original-title="" title="">
								</a>
							
							</div>
						</div>
						<div class="portlet-body form">
							
							

		<form action="/admin/holiday/save" method="post" class="j-forms" id="j-forms" novalidate>
        
<input type="hidden" name="company_id" id="company_id" value="{{$current_user->company_id}}"/>
 <input type="hidden" name="id" id="id" value="@if(isset($user)){{$user->id}}@endif" />
		{!! csrf_field() !!}

			<div class="content">

				<!-- start name -->
				<div class="j-row">
					
					<div class="span12 unit">
                    <label class="label">Name</label>
						<div class="input">
							<label class="icon-right" for="name">
								<i class="fa fa-user"></i>
							</label>
                            
							<input type="text" id="name" name="name" @if(isset($user)) value="{{$user->name}}@endif" >
						</div>
					</div>
				</div>
				<!-- end name -->

				<!-- start Estimated Dates -->
				<div class="j-row">
					<div class="span6 unit">
						<label class="label">Start date</label>
						<div class="input">
							<label class="icon-right" for="date_from">
								<i class="fa fa-calendar"></i>
							</label>
							<input type="text" id="start_date" name="start_date" class="datePicker" @if(isset($user)) value="{{$user->start_date}}" @endif>
						</div>
					</div>
					<div class="span6 unit">
						<label class="label">End date</label>
						<div class="input">
							<label class="icon-right" for="date_to">
								<i class="fa fa-calendar"></i>
							</label>
							<input type="text" id="end_date" name="end_date" class="datePicker" @if(isset($user)) value="{{$user->end_date}}" @endif>
						</div>
					</div>
                    
				</div>
				<!-- end Estimated Dates -->

				<!-- start users -->
                <div class="j-row">
						<label class="label">Employees</label>
						<div class="span3">
                        @foreach($users as $user)
							<label class="checkbox">
								<input type="checkbox" checked="checked" name="users[]" value="{{$user->id}}">
								<i></i>
								{{$user->name}}
							</label>
                            @endforeach
							
						</div>
						
						
						
					</div>
				<!-- end users -->

				<!-- start password --><!-- end password -->


				<!-- start response from server -->
			  <div id="response" style="display:none;"></div>
				<!-- end response from server -->

			</div>
			<!-- end /.content -->

			<div class="footer">
				<button type="submit" class="primary-btn" id="enable-button" >Submit</button>
			</div>
			<!-- end /.footer -->

		</form>
	
						</div>
						<!-- END VALIDATION STATES-->
					</div>
					<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Success</h4>
										</div>
										<div class="modal-body">
											 Successfully registered
										</div>
										<div class="modal-footer">
											<button type="button" class="btn default" data-dismiss="modal">Close</button>
											
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>