
<div class="portlet box grey-cascade">
	<div class="portlet-title">
		<div class="caption">
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse">
			</a>
			
			
			
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-toolbar">
			<div class="row">
				<div class="col-md-6">
					<div class="btn-group">
						<a href="javascript: taps.loadajaxpage('company/add')" >
						<button id="sample_editable_1_new" class="btn green" style="float:none;">
						Add New <i class="fa fa-plus"></i>
						</button>
					</a>
                    <button type="button" class="btn red delete_multiple" disabled="disabled" style="float:none;" data-model="{{$model_name}}" ><i class="fa fa-remove"></i> Delete</button>
					</div>
			  </div>
				<div class="col-md-6"></div>
			</div>
		</div>
        <form name="vpb_form_name" id="deleteMult" method="post" action="">
        {!! csrf_field() !!}
		<table class="table table-striped table-bordered table-hover" id="sample_1">
		<thead>
		<tr class="tr-head">
			<th class="table-checkbox">
				<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" name="stuff-select[]"/>
			</th>
			<th>
				 Company name
			</th>
			<th>
				 Actions
			</th>
		</tr>
		</thead>
		<tbody>
        @foreach($users as $user)
		<tr class="odd gradeX" id="data-row-{{$user->id}}">
			<td valign="middle">
				
                <input type="checkbox" name="users[]"  class="checkboxes"  value="{{$user->id}}" >
			</td>
			<td valign="middle">
				 {{$user->name}}
		  </td>
			<td valign="middle"><a href="javascript: taps.loadajaxpage('company/{{$user->id}}')"><button type="button" class="btn green"><i class="fa fa-edit"></i> Edit</button></a>
                                <button type="button" id="{{$user->id}}" data-model="{{$model_name}}" class="btn red delete"><i class="fa fa-remove"></i> Delete</button></td>
		</tr>
        @endforeach
		</tbody>
		</table>
        </form>
	</div>
</div>