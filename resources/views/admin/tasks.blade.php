@extends('admin.master')
<?php $i=2; $j =2;?>
@section('table_name')
tasks
@stop
@section('add_css')
	<link rel="stylesheet" type="text/css" href="/assets/global/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
    <link href="/assets/global/plugins/dropzone/css/dropzone.css" rel="stylesheet"/>

@stop

@section('add_js_plugins')
	<script src="/assets/global/plugins/dropzone/dropzone.js"></script>
	<script type="text/javascript" src="/assets/global/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
	<script src="/assets/admin/pages/scripts/form-dropzone.js"></script>
@stop

@section('add_js_scripts')
	<script src="/assets/admin/pages/scripts/table-managed.js"></script>
	<script type="text/javascript">
		/***************************************/
		/* After ajax is loaded  */
		/***************************************/
		taps.onajaxpageload=function(pageurl){
			/***************************************/
			/*check boxes for task progress  */
			/***************************************/
			$(".cb-enable").click(function(){
				var parent = $(this).parents('.switch');
				$('.cb-disable',parent).removeClass('selected');
				$(this).addClass('selected');
				$('.checkbox',parent).attr('checked', true);
				taskId = $(this).attr('data-id');
				token = $('#hidden_attributes').attr('data-token');
				$.post("/admin/updateTaskProgress",{task_id:taskId,progress:100,_token:token} ,function(data, status){
					
				});
				
			});
			$(".cb-disable").click(function(){
				taskId = $(this).attr('data-id');
				token = $('#hidden_attributes').attr('data-token');
				var parent = $(this).parents('.switch');
				$('.cb-enable',parent).removeClass('selected');
				$(this).addClass('selected');
				$('.checkbox',parent).attr('checked', false);

				$.post("/admin/updateTaskProgress",{task_id:taskId,progress:0,_token:token} ,function(data, status){
				});
			});
		
			/***************************************/
			/* Checkboxes handling */
			/***************************************/
			var checkboxes = $("input[type='checkbox']"),
    		submitButt = $(".delete_multiple");
			checkboxes.click(function() {
				submitButt.attr("disabled", !checkboxes.is(":checked"));
			});
			
			/***************************************/
			/* Show back button */
			/***************************************/
			$("#back-btn").show();
			$("#back-btn").attr("href","javascript: taps.loadajaxpage('/admin/getTasks');") ;
			
			$(document).ready(function(){

	// Start date
	
		$( ".datePicker" ).datepicker({
			dateFormat: 'yy-mm-dd',
			prevText: '<i class="fa fa-caret-left"></i>',
			nextText: '<i class="fa fa-caret-right"></i>'
			
		});
		$('.datetimePicker').datetimepicker({
				prevText: '<i class="fa fa-caret-left"></i>',
				nextText: '<i class="fa fa-caret-right"></i>',
				dateFormat: 'yy-mm-dd',
				timeFormat: 'HH:mm',
		});
	

				/***************************************/
				/* Form validation */
				/***************************************/
				$( '#j-forms' ).validate({
			
					/* @validation states + elements */
					errorClass: 'error-view',
					validClass: 'success-view',
					errorElement: 'span',
					onkeyup: false,
					onclick: false,
			
					/* @validation rules */
					rules: {
						project_name: {
							required: true
						},
						client_id: {
							required: false
						},
						domain_name: {
							required: false
						},
						old_url: {
							required: false
						},
						project_logo: {
							required: false
						},
						est_start_date: {
							required: false
						},
						act_start_date: {
							required: false
						},
						est_end_date: {
							required: false
						},
						act_end_date: {
							required: false
						},
						project_remarks: {
							required: false
						},
						company_id: {
							required: false
						}
					},
					messages: {
						name: {
							required: 'Please enter a name'
						},
						email: {
							required: 'Please enter your email',
							email: 'Incorrect email format'
						},
						password: {
							required: 'Please enter your password',
							minlength: 'At least 6 characters'
						}
					},
					// Add class 'error-view'
					highlight: function(element, errorClass, validClass) {
						$(element).closest('.input').removeClass(validClass).addClass(errorClass);
						if ( $(element).is(':checkbox') || $(element).is(':radio') ) {
							$(element).closest('.check').removeClass(validClass).addClass(errorClass);
						}
					},
					// Add class 'success-view'
					unhighlight: function(element, errorClass, validClass) {
						$(element).closest('.input').removeClass(errorClass).addClass(validClass);
						if ( $(element).is(':checkbox') || $(element).is(':radio') ) {
							$(element).closest('.check').removeClass(errorClass).addClass(validClass);
						}
					},
					// Error placement
					errorPlacement: function(error, element) {
						if ( $(element).is(':checkbox') || $(element).is(':radio') ) {
							$(element).closest('.check').append(error);
						} else {
							$(element).closest('.unit').append(error);
						}
					},
					// Submit the form
					submitHandler:function() {
						$( '#j-forms' ).ajaxSubmit({
			
							// Server response placement
							target:'#j-forms #response',
							
							// If error occurs
							error:function(xhr) {
								//$('#j-forms #response').html('An error occured: ' + xhr.status + ' - ' + xhr.statusText);
								msg = "There was an error"	;
									title = "Failed";	
									theme ="error";
								var $toast = toastr[theme](title, msg);
								$('#j-forms button[type="submit"]').attr('disabled', false).removeClass('processing');
							},
			
							// Before submiting the form
							beforeSubmit:function(){
								// Add class 'processing' to the submit button
								$('#j-forms button[type="submit"]').attr('disabled', true).addClass('processing');
							},
			
							// If success occurs
							success:function(xhr){
								//alert(xhr);
									msg = "Successfully Saved"	;
									title = "Congratulations";
									theme ="success";
								var $toast = toastr[theme](title, msg); // Wire up an event handler to a button in the toast, if it exists
								$( "#multipleUpload" ).show( "slow", function() {
									$(document).scrollTop( $("#multipleUpload").offset().top );  
							 	 });
								 $("#stuff_id").val(xhr);
								 $("#id").val(xhr);
								// Remove class 'processing'
								$('#j-forms button[type="submit"]').attr('disabled', false).removeClass('processing');
			
								// Remove classes 'error-view' and 'success-view'
								$('#j-forms .input').removeClass('success-view error-view');
								$('#j-forms .check').removeClass('success-view error-view');
			
								// If response from the server is a 'success-message'
								if ( $('#j-forms .success-message').length ) {
			
									// Reset form
									$('#j-forms').resetForm();
			
									// Make checkbox 'terms of use' unavailable
									$('#check-enable-button').attr('checked', false);
									$('#check-enable-button').attr('disabled', true);
			
									// Make 'submit' button unavailable while checkbox doesn't checked
									$('#enable-button').attr('disabled', true).addClass('disabled-view');
			
									setTimeout(function(){
										// Delete success message after 5 seconds
										$('#j-forms #response').removeClass('success-message').html('');
			
										// Make checkbox 'terms of use' available
										$('#check-enable-button').attr('disabled', false);
									}, 5000);
								}
							}
						});
					}
				});
				/***************************************/
				/* end form validation */
				/***************************************/
});
			/***************************************/
			/* Inistantiate data tables */
			/***************************************/
			TableManaged.init();
			/***************************************/
			/* Inistantiate Modal plugin to confirm deleting */
			/***************************************/
			UIAlertDialogApi.init();
			/***************************************/
			/* Inistantiate Modal plugin to Multiple upload */
			/***************************************/
			dropzoneAPI.init();
			
			/***************************************/
			/* updating task order & dependency list after selecting a project */
			/***************************************/
			$("#project_id").change(function(){
				projectId = $(this).val();
				
				token = $('#hidden_attributes').attr('data-token');
				$.post("/admin/getTaskMaxOrder",{project_id:projectId,_token:token} ,function(data, status){
					returnData = jQuery.parseJSON(data);
					tasks = returnData.tasks;
					var items = [];
     
    				items.push('<option value=""></option>');
					$.each(tasks,function(key, value) 
					{
						console.log(value.task_name);
						items.push('<option value=' + value.id + '>' + value.task_name + '</option>');
						
					});
					$(".dependency_id").html(items.join(''));
					$('.dependency_id').val('').trigger('chosen:updated');
					
					currentId = $("#id").val();
					if(currentId == ""){
						$("#task_order").val(returnData.order);
						
					}
				});
			});
			var stuff_id  = $("#id").val();
			var task_progress = 0;
			if(stuff_id!=""){
				task_progress = $("#task_progress").val();
			}
			/***************************************/
			/* task progress slider */
			/***************************************/
			$( '#slider-1-h' ).slider({
				range: "min",
				min: 0,
				max: 100,
				value: task_progress,
				slide: function( event, ui ) {
					$( '#1-h' ).html( ui.value+" %"  );
					$( '#task_progress' ).val( ui.value );
				}
			});
			$( '#1-h' ).html( $( '#slider-1-h' ).slider( 'value' )+" %" );
			
			


		}
		
	</script>
@stop
@section('add_inits')
	
	
@stop
@section('title')
	Tasks
@stop

@section('page_title')
	Tasks
@stop

@section('page_title_small')
	
@stop

@section('content')

<div id="countrytabs" class="shadetabs" style="display:none;" >
    <li><a href="/admin/getTasks" rel="countrycontainer">All users</a></li>
</div>

@stop



