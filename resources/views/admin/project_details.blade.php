
<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i></div>
							<div class="tools">
								<a href="javascript:;" class="collapse" data-original-title="" title="">
								</a>
							
							</div>
						</div>
						<div class="portlet-body form">
							
							

		<form action="/admin/project/save" method="post" class="j-forms" id="j-forms" novalidate>
        <input type="hidden" name="company_id" id="company_id" value="{{$current_user->company_id}}"/>
 <input type="hidden" name="id" id="id" value="@if(isset($user)){{$user->id}}@endif" />
		{!! csrf_field() !!}

			<div class="content">
				<!-- start Client -->
				  <div class="j-row">
				<div class="unit">
					<label class="input select">
						<select name="client_id">
							<option value="none" selected="" disabled="">Select Client</option>
                            @foreach($clients as $client)
							<option @if(isset($user))  @if( $user->client_id == $client->id) selected  @endif @endif value="{{$client->id}}">{{$client->name}}</option>
                            @endforeach
						</select>
						<i></i>
					</label>
				</div>
            </div>
				<!-- end Client -->
              
				<!-- start name -->
				<div class="j-row">
					
					<div class="span12 unit">
                    <label class="label">Name</label>
						<div class="input">
							<label class="icon-right" for="name">
								<i class="fa fa-globe"></i>
							</label>
							<input type="text" id="name" name="project_name" @if(isset($user)) value="{{$user->project_name}}" @endif>
						</div>
					</div>
				</div>
				<!-- end name -->

				<!-- start Estimated Dates -->
				<div class="j-row">
					<div class="span6 unit">
						<label class="label">Estimated start date</label>
						<div class="input">
							<label class="icon-right" for="date_from">
								<i class="fa fa-calendar"></i>
							</label>
							<input type="text" id="est_start_date" name="est_start_date" class="datePicker" @if(isset($user)) value="{{$user->est_start_date}}" @endif>
						</div>
					</div>
					<div class="span6 unit">
						<label class="label">Estimated end date</label>
						<div class="input">
							<label class="icon-right" for="date_to">
								<i class="fa fa-calendar"></i>
							</label>
							<input type="text" id="est_end_date" name="est_end_date" class="datePicker" @if(isset($user)) value="{{$user->est_end_date}}" @endif>
						</div>
					</div>
				</div>
				<!-- end Estimated Dates -->
				<!-- start Actual Dates -->
				<div class="j-row">
					<div class="span6 unit">
						<label class="label">Actual start date</label>
						<div class="input">
							<label class="icon-right" for="date_from">
								<i class="fa fa-calendar"></i>
							</label>
							<input type="text" id="act_start_date" name="act_start_date" class="datePicker" @if(isset($user)) value="{{$user->act_start_date}}" @endif> 
						</div>
					</div>
					<div class="span6 unit">
						<label class="label">Actual end date</label>
						<div class="input">
							<label class="icon-right" for="date_to">
								<i class="fa fa-calendar"></i>
							</label>
							<input type="text" id="act_end_date" name="act_end_date" class="datePicker" @if(isset($user)) value="{{$user->act_end_date}}" @endif>
						</div>
					</div>
				</div>
				<!-- end Actual Dates -->
                <!-- start Deadline -->
				<div class="j-row">
					
					<div class="span12 unit">
						<label class="label">Deadline date</label>
						<div class="input">
							<label class="icon-right" for="date_to">
								<i class="fa fa-calendar"></i>
							</label>
							<input type="text" id="deadline_date" name="deadline_date" class="datePicker" @if(isset($user)) value="{{$user->deadline_date}}" @endif>
						</div>
					</div>
				</div>
				<!-- end Deadline-->
				<!-- start Domain & URL -->
				<div class="j-row">
					<div class="span6 unit">
						<label class="label">Domain Name</label>
						<div class="input">
							<label class="icon-right" for="date_from">
								<i class="fa fa-globe"></i>
							</label>
							<input type="text" id="domain_name" name="domain_name" @if(isset($user)) value="{{$user->domain_name}}" @endif>
						</div>
					</div>
					<div class="span6 unit">
						<label class="label">Old URL</label>
						<div class="input">
							<label class="icon-right" for="date_to">
								<i class="fa fa-globe"></i>
							</label>
							<input type="text" id="old_url" name="old_url" @if(isset($user)) value="{{$user->old_url}}" @endif >
						</div>
					</div>
				</div>
				<!-- end Domain & URL-->
                <!-- start Remarks -->
				<div class="j-row">
					
					<div class="span12 unit">
						<label class="label">Remarks</label>
						<div class="input">
							                <textarea placeholder="Remarks" spellcheck="true" name="project_remarks">@if(isset($user)) {{$user->project_remarks}} @endif</textarea>

						</div>
					</div>
				</div>
				<!-- end Remarks-->


				<!-- start response from server -->
				<div id="response" style="display:none;"></div>
				<!-- end response from server -->

			</div>
			<!-- end /.content -->
			<div class="footer">
				<button type="submit" class="primary-btn" id="enable-button" >Submit</button>
			</div>
			<!-- end /.footer -->

		</form>
        
        
	
						</div>
						<!-- END VALIDATION STATES-->
					</div>
					<div class="portlet box blue" @if(!isset($user)) style="display:none;" @endif id="multipleUpload">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-file"></i>&nbsp; Project Files</div>
							<div class="tools">
								<a href="javascript:;" class="collapse" data-original-title="" title="">
								</a>
							
							</div>
						</div>
                        <div class="portlet-body form">
                        <form action="/admin/multipleUpload" class="dropzone" id="my-dropzone"   enctype="multipart/form-data">
                        {!! csrf_field() !!}
                         <input type="hidden" name="stuff_id" id="stuff_id" value="@if(isset($user)){{$user->id}}@endif" />
                         <input type="hidden" name="table_name" value="" id="table_name" />

                        </form>
                        </div>
                        
                        </div>
                        
             