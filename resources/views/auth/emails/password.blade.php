
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="#364150">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" bgcolor="#eceef1" style="color:#364150;">Click here to reset your password: <a style="color:#4db3a5;" href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a></td>
  </tr>
  <tr>
    <td bgcolor="#364150">&nbsp;</td>
  </tr>
</table>
